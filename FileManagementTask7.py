# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 19:12:59 2020

@author: mkhude
"""
#Windows
#!/usr/bin/env python3
import os
import platform
import json
username=input("Enter your name")
if platform.system() == "Windows":
    root_path="c:\py_folders"
    path="c:\py_folders\processing\\archive"
    folders=['cache','processing\import',path,'config']
    try:
        for folder in folders:
            os.makedirs(os.path.join(root_path, folder),exist_ok=False)
    except OSError:
        print ("Creation of the directory %s failed File exists" % root_path)
    else:
        print ("Successfully created the directory %s" % root_path)

    os_type=platform.system()
    os.chdir("c:\py_folders\config")
    result={"Name" : "PyFolders","user" : username, "OS" : os_type}
    
    #new_file=open("newfile.txt",mode="w",encoding="utf-8")
    with open("Config.json","w",encoding='utf-8') as f:
        json.dump(result,f,indent=4,ensure_ascii=False)
elif platform.system() == "Linux":
    root_path="\opt\py_folders"
    os.mkdir(root_path)
    os.chdir("\opt\py_folders")
    for dir in ('cache','processing','config'):
        os.mkdir(dir)
    os.chdir("/opt/py_folders/processing")
    for dir in ('import','archive'):
        os.mkdir(dir)
    
    os_type=platform.system()             
    result={"Name" : "PyFolders","user" : username, "OS" : os_type}
    
    #new_file=open("newfile.txt",mode="w",encoding="utf-8")
    with open("Config.json","w",encoding='utf-8') as f:
        json.dump(result,f,indent=4,ensure_ascii=False)
else:
    print("You are trying on another system")
