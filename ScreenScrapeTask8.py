# -*- coding: utf-8 -*-
"""
Created on Thu Oct 29 08:34:22 2020

@author: ManaliKhude
"""
import requests
import json
import time
stime=time.localtime()
formatted_time=time.strftime("%m-%d-%Y,%H:%M:%S",stime)
url=requests.get("https://velocitycloud.com")
#print(url) response 200
#print(url.headers) get headers
url.encoding="utf-8"
header=dict(url.headers)
response=requests.head("https://velocitycloud.com")#checking redirection
h2="X-Powered-By"
h1="Content-Security-Policy"
if response.status_code == 301:
    httpsredirect="redirected"
else:
    httpsredirect="not Redirected"

if url.status_code == 200:
    http200 = "True"
else:
    http200 = "False"

# =============================================================================
# if h1 == 'upgrade-insecure-requests':
#     h1out = "True"
# else:
#     h1out = "False"
# 
# if h2 == 'HubSpot':
#     h2out = "True"
# else:
#     h2out = "False"    
# =============================================================================

#count the word velocity
count=requests.get("https://velocitycloud.com").text.lower().count('Velocity' and 'velocity')    
Output = {"DateTime" : formatted_time, "https_redirect" : str(response.status_code)+","+httpsredirect, 
            "http_response":str(url.status_code)+","+http200,"httpheader_content-security-policy":header[h1],
            "http_header_x-powered-by":header[h2],"screen_scrape_wordcount": str(count)}

with open("Sample.json","w",encoding='utf-8') as f:
    json.dump(Output,f,indent=4,ensure_ascii=False)
    
